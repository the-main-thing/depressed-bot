import logging
import random

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from telegram_token import TOKEN

updater = Updater(token=TOKEN)
dispatcher = updater.dispatcher

logging_format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(format=logging_format, level=logging.INFO)

handlers = []

def start(bot, update):
    bot.send_message(
                    chat_id=update.message.chat_id,
                    text='Привет.\nГовори.\nЕсли нужна помощь, пиши /help.'
                    )
handlers.append(CommandHandler('start', start))

def talk(bot, update):
    answers = [
               'Сам ты _',
               'От _ слышу!',
               '_ на заборе написано.',
               '_ идеально подходит к твоему цвету глаз',
               'Павлик тебя найдёт, не сомневайся _',
               'Na zdoroviye!'
              ]
    index = random.randrange(0, 6, 1)
    text = answers[index].replace('_', update.message.text)
    bot.send_message(
                    chat_id=update.message.chat_id,
                    text=text
                    )
handlers.append(MessageHandler(Filters.text, talk))

def reverse(bot, update, args):
    if args:
        result = [arg[::-1] for arg in args]
        result = ' '.join(result)
    else:
        result = 'Ты всё неправильно делаешь. Надо так:\n/reverse ыт хол'
    bot.send_message(
                    chat_id=update.message.chat_id,
                    text=result
                    )
handlers.append(CommandHandler('reverse', reverse, pass_args=True))

def help_command(bot, update):
    text = ('Можно писать слова и бот будет отвечать. На некоторые слова' +
    ' не так как на все остальные. Ещё есть команда /reverse.' +
    ' Она разворачивает слова, не меняя их порядок.' +
    ' Только со знаками препинания не справляется. Попробуй.')

    bot.send_message(chat_id=update.message.chat_id,
                     text=text)
handlers.append(CommandHandler('help', help_command))

def get_chat_id(bot, update):
    bot.send_message(chat_id=update.message.chat_id,
                     text=str(update.message.chat_id)
                     )
handlers.append(CommandHandler('get_chat_id', get_chat_id))

# register handlers
for handler in handlers:
    dispatcher.add_handler(handler)

# start bot
updater.start_polling()
