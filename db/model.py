from sqlalchemy import create_engine, Column, Integer
from sqlalchemy.ext.declarative import declarative_base


engine = create_engine('sqlite:///depress.db')
Base = declarative_base()
